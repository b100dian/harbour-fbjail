import QtQuick 2.6
import Sailfish.Silica 1.0

CoverBackground {
    id: coverPage

    Image {
        source: "./background.png"
        anchors {
            right: parent.right
            bottom: parent.bottom
        }
        width: parent.width * 4/5
        fillMode: Image.PreserveAspectFit
        opacity: 0.2
    }

    CoverPlaceholder {
        text: Qt.application.name
        textColor: Theme.highlightColor
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
