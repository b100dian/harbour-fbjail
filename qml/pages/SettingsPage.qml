import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
    SilicaFlickable{
        anchors.fill: parent
        Column {
            spacing: Theme.paddingMedium
            width: parent.width - Theme.horizontalPageMargin
            PageHeader{ title:  Qt.application.name + " " + qsTr("Settings", "page title") }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.hideui
                text: qsTr("Hide UI in landscape mode")
                description: qsTr("If enabled, the top and bottom bars are hidden in landscape mode.")
            }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.remember
                text: qsTr("Remember last page")
                description: qsTr("If enabled, the last visited page will be loaded after application (re)start.")
            }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.chronoSort
                text: qsTr("Sort chronologically")
                description: qsTr("If enabled, the order of posts will not be suggested by facebook, but will be chronological")
            }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.nag
                text: qsTr("Show healthy reminders")
                description: qsTr("If enabled, some parts of the UI will show helpful reminders about Facebook usage.")
            }
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Page Zoom")
            }
            Slider{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                label: qsTr("Zoom factor")
                minimumValue: 0.8
                maximumValue: 5.2
                stepSize: 0.2
                value: app.zoom
                valueText: "x" + value
                onReleased: app.zoom = sliderValue
            }
            Label{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
                text: qsTr("Restart the App to apply zoom factor changes.")
            }
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
