import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
    SilicaFlickable {
        anchors.fill: parent
        contentHeight: aboutColumn.height

        VerticalScrollDecorator {}

        Column {
            id: aboutColumn
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingLarge
            bottomPadding: Theme.itemSizeMedium

            PageHeader { title: qsTr("About") + Qt.application.name + " " + Qt.application.version }

            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                source: Qt.resolvedUrl("../images/icon.svg")
                sourceSize.width: parent.width /3
                width: sourceSize.width
                fillMode: Image.PreserveAspectFit
            }

            SectionHeader { text: qsTr("What's %1").arg(Qt.application.name) }
            Label {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignJustify
                text: qsTr("%1 is an unofficial Facebook client for Sailfish OS.").arg(Qt.application.name)
            }

            Label {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignJustify
                text: qsTr("It is in fact a simple combination of Sailfish OS features WebView and SailJail so your FB browsing is separated from your other browsing activities.")
            }

            Label {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignJustify
                text: qsTr("Hopefully this will improve your privacy a little bit - although of course Facebook/Meta are world-class in spying so additional measures are required for this to actually be effective.")
            }

            SectionHeader { text: qsTr("License") }
            Label {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignHCenter
                textFormat: Text.StyledText
                text: "<p>" + qsTr("%1 is free software released under the MIT License.").arg(Qt.application.name) + "</p>"
                    + "<p>" + qsTr("WebView and SailJail are developed by Jolla and the Sailfish OS community.") + "</p>"
                    + "<p>" + qsTr("WebView includes software developed by Mozilla and is licensed under the Mozilla Public License 2.0") + "</p>"
                    + "<p>" + qsTr("SailJail is licensed under the 3-Clause BSD license and is based on Firejail developed by the NetBlue project licensed under the GPL 2.0") + "</p>"
            }

            SectionHeader { text: qsTr("Source Code") }
            Label {
                width: parent.width /2
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                textFormat: Text.StyledText
                linkColor: Theme.highlightColor
                onLinkActivated: Qt.openUrlExternally(link)
                text: qsTr("available on <a href='https://gitlab.com/nephros/harbour-fbjail'>GitLab</a>")
            }

            SectionHeader { text: qsTr("Developers") }
            Label {
                width: parent.width /2
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                textFormat: Text.StyledText
                linkColor: Theme.highlightColor
                onLinkActivated: Qt.openUrlExternally(link)
                text: "<a href='mailto:sailfish@nephros.org?bcc=sailfish+fratzenjail@nephros.org&subject=A%20message%20from%20a%20" + Qt.application.name + "%20user&body=Hello%20nephros%2C%0A'>Peter G. (nephros)</a>"
            }
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
