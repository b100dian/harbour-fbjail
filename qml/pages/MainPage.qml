import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.WebView 1.0
import Sailfish.WebView.Popups 1.0
import Sailfish.WebEngine 1.0
import Nemo.Notifications 1.0
import "../components"
import "../javascript/facebook.js" as FB

WebViewPage {
    id: page

    /* properties */
    property bool enginesRunning: false
    // emulate an Enum ;)
    readonly property QtObject stateNames: QtObject {
        readonly property string basic: "basic"
        readonly property string mobile: "mobile"
        readonly property string desktop: "full"
    }
    /* state */
    states: [
        State {
            name: stateNames.basic
            PropertyChanges { target: webview;
                baseurl: "mbasic.facebook.com";
                httpUserAgent: browser.mobileUA
                desktopMode: false
            }
            PropertyChanges { target: header; title: qsTr("Basic", "page display mode"); leftIcon: "image://theme/icon-m-website" }
        },
        State {
            name: stateNames.mobile
            PropertyChanges { target: webview;
                baseurl: "m.facebook.com" ;
                httpUserAgent: browser.mobileUA
                desktopMode: false
            }
            PropertyChanges { target: header; title: qsTr("Mobile", "page display mode"); leftIcon: "image://theme/icon-m-device" }
        },
        State {
            name: stateNames.desktop
            PropertyChanges { target: webview;
                baseurl: "www.facebook.com";
                httpUserAgent: browser.fullUA
                desktopMode: true
            }
            PropertyChanges { target: header; title: qsTr("Desktop", "page display mode"); leftIcon: "image://theme/icon-m-computer" }
        }
    ]

    /* connections */
    Connections {
        target: WebEngineSettings
        onInitializedChanged: {
            if (WebEngineSettings.initialized === true ) {
                console.debug(  "Settings initialized" );
                if (!enginesRunning) { startEngines() } else { console.debug("Engines pumping.") }
            } else {
                console.debug(  "Settings not yet initialized" );
            }
        }
    }

    /* functions */
    function replaceUrl(n) {
        //var u = Qt.resolvedUrl(n);
        //console.debug( "Replaced url is ..." + u );
        //webview.url = u;
        webview.url = Qt.resolvedUrl(n);
    }
    function replaceUri(n) {
        var re = /^[htps]{3,5}\:\/{2}/;
        //console.debug( "Got a link ..." + n );

        // is it an uri or full string?
        if (re.test(n.toString())) {
            replaceUrl(n);
            return;
        } else {
            var u = webview.baseurl ? "https://" + webview.baseurl + n : "https://" + "m.facebook.com" + n;
            replaceUrl(u);
            return;
        }
        console.warn("We should never have got here!");
    }
    function getExternalUrl(url) {
        return decodeURI(url);
    }
    function startEngines() {
        console.debug(  "starting Engines..." );
        WebEngineSettings.autoLoadImages    = true;
        WebEngineSettings.cookieBehavior    = WebEngineSettings.BlockThirdParty;
        WebEngineSettings.downloadDir       = StandardPaths.downloads + "/Facebook/";
        WebEngineSettings.javascriptEnabled = true;
        WebEngineSettings.useDownloadDir    = true;
        //WebEngineSettings.pixelRatio        = (1.8*Theme.pixelRatio*10)/10.0
        WebEngineSettings.pixelRatio        = (app.zoom*Theme.pixelRatio*10)/10.0

        /* Mozilla Preferences */
        WebEngineSettings.setPreference("general.useragent.updates.enabled",        false);
        WebEngineSettings.setPreference("intl.accept_languages",                    "en-IE, en-GB, en-US, en");
        WebEngineSettings.setPreference("javascript.use_us_english_locale",         true);
        WebEngineSettings.setPreference("browser.startup.blankWindow",              false);
        WebEngineSettings.setPreference("browser.display.use_system_colors",        true);
        WebEngineSettings.setPreference("media.autoplay.blocking_policy",           2);
        enginesRunning = true;
    }

    /* components */
    PullDownMenu{
        flickable: flick
        //visible: webview.loaded
        //MenuItem{text: qsTr("Log out"); onClicked: { replaceUri("/logout.php") }}
        MenuItem{text: qsTr("About"); onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))}
        MenuItem{text: qsTr("Settings"); onClicked: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))}
    }
    SilicaFlickable {
        id: flick
        anchors.fill: parent

        TopBar{ id: header}
        RightPanel { id: rightPage ; anchors.top: header.bottom; z: 5; inTitle: webview.title; inLink: webview.url.toString() }
        BarMenu { id: barMenu;  anchors.top: header.bottom; z: 4 }
        WebView {
            id: webview
            property string baseurl

            anchors.top: header.bottom
            width: parent.width
            height: viewportHeight

            /* webview specific */
            viewportHeight: ((page.orientation & Orientation.PortraitMask) ? Screen.height : Screen.width) - header.height - footer.height
            httpUserAgent: browser.mobileUA
            privateMode: false
            chromeGestureEnabled: true
            /* popups */
            popupProvider: PopupProvider {
                contextMenu: ({"type": "item", "component": "qrc:/popups/CustomContextMenu.qml" })
                //contextMenu: Component { CustomContextMenu {} }
                //contextMenu: Component { ContextMenu { downloadsEnabled: true } }
                //contextMenu: Qt.createComponent("CustomContextMenu.qml");
                //contextMenu: "qrc:/popups/CustomContextMenu.qml"
            }

            /* signals */
            Component.onCompleted: { url = (app.remember && app.savedUrl ) ? browser.loadSavedUrl() : "https://facebook.com/home.php?sk=" + (app.chronoSort? "h_chr" : "") }
            onBaseurlChanged: { url = setBaseUrl(); }
            onStateChanged: { console.debug("State now: ", state); }
            onUrlChanged: {
                FB.scrutinizeLink(url);
                console.debug("Loading ", url);
            }

            onDesktopModeChanged: {
                // TODO: respect user setting for zoom
                if (parent.enginesRunning === true){
                    console.debug("WES.pixelRatio was:" + WebEngineSettings.pixelRatio + ", Theme.pixelRatio is " + Theme.pixelRatio)
                    if (desktopMode === true ) {
                        WebEngineSettings.pixelRatio = (2 * app.zoom * Theme.pixelRatio*10)/10.0
                    } else {
                        WebEngineSettings.pixelRatio = (app.zoom * Theme.pixelRatio*10)/10.0
                    }
                    console.debug("WES.pixelRatio is now:" + WebEngineSettings.pixelRatio);
                }
            }
            onActiveChanged: {
                if (!active && app.remember && loaded && url) {
                    console.debug("Saving url...");
                    app.savedUrl = browser.saveUrl(url);
                }
            }
            onLinkClicked: FB.scrutinizeLink(url)

            /* functions */
            // switch host part of URL with baseurl one
            function setBaseUrl() {
                console.debug("Got url " + url);
                // QML url is a not a string, see https://doc.qt.io/qt-5/qml-url.html
                if (url.toString() != "") {
                    var old = url.toString();
                    var u = old.replace(/^http.?\:\/{2}[a-zA-Z0-9_.:-]+/, "https://" + baseurl);
                    if (page.state === stateNames.desktop) {
                        if ( u.indexOf("?") === -1 ) {
                            u += "?m2w";
                        } else {
                            u += "&m2w";
                        }
                    }
                    console.debug("Replaced url " + url + " with " + u);
                    return Qt.resolvedUrl(u);
                } else {
                    return Qt.resolvedUrl("https://" + baseurl);
                }
            }
        }
        BottomBar {id: footer}
    }
}

 // vim: ft=javascript expandtab ts=4 sw=4 st=4
