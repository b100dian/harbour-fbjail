import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import "cover"
import "pages"

ApplicationWindow {
    id: app

    property alias savedUrl: browser.savedUrl
    property alias hideui: config.hideui
    property alias remember: config.remember
    property alias nag: config.nag
    property alias zoom: config.zoom
    property alias chronoSort: config.chronoSort

    Component.onCompleted: {
        Qt.application.domain  = "sailfish.nephros.org";
        Qt.application.version = "unreleased";
        console.info("Intialized", Qt.application.name, "version", Qt.application.version, "by", Qt.application.organization );
        console.debug("Parameters: " + Qt.application.arguments.join(" "))
    }
    ConfigurationGroup  {
        id: settings
        path: "/org/nephros/" + Qt.application.name
    }
    ConfigurationGroup  {
        id: config
        scope: settings
        path:  "app"
        property bool hideui: true
        property bool remember: true
        property bool nag: true
        property double zoom: 2.8
        property bool chronoSort: true
    }
    ConfigurationGroup  {
        id: browser
        scope: settings
        path:  "browser"
        readonly property string mobileUA: "Mozilla/5.0 (Maemo; Linux; U; Jolla; Sailfish; Android 2.3.5) AppleWebKit/538.1 (KHTML, like Gecko) Version/5.1 Chrome/30.0.0.0 Mobile Safari/538.1 (compatible)" + Qt.application.name + "/" + Qt.application.version
        //readonly property string tabletUA: "Mozilla/5.0 (Sailfish 4.0; Tablet; rv:60.0) Gecko/60.0 Firefox/60.0 " + Qt.application.name + "/" + Qt.application.version
        readonly property string fullUA:   "Mozilla/5.0 (Wayland; Linux; rv:60.0) Gecko/20100101 Firefox/60.0 " + Qt.application.name + "/" + Qt.application.version
        property string userUA: ""
        property string savedUrl: ""
        // TODO: obfuscate because privacy. atob/btoa are NOT available
        function saveUrl(u) { savedUrl = u; }
        function loadSavedUrl() { return Qt.resolvedUrl(savedUrl); }
    }
    cover: Component { CoverPage{} }
    initialPage: Component { MainPage{} }
}

 // vim: ft=javascript expandtab ts=4 sw=4 st=4
