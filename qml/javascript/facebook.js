/*
 *
 * ======== Globals  ========
 *
 */

// thanks, sailbook!
var facebookExtLinkRegex = /lm\.facebook.com\/l\.php\?u=(.*)&h=/;
var facebookIntLinkRegex = /(?:m\.|mbasic\.|web\.|www\.)facebook.*\//;
// from https://megamorf.gitlab.io/2018/12/02/facebook-fbclid-param-breaks-website/
var facebookFbclidRegex = /^(.*)&?fbclid=[^&]+&?(.*)$/;

var protoRegex  = /^[htps]{3,5}\:\/{2}/;
var contentRegex = /fbcdn.net/;
/*
 *
 * ======== URL mangling  ========
 *
 */


// TODO: contain links to FB, handle external Links via OpenUrlExternal or Share()
function scrutinizeLink(link) {
    var l = link.toString();
    console.debug("scrutinizing: " + l);
    // is it a proper link, not "about:" and others...
    if (!protoRegex.test(l)) {
        console.debug("result: not a httpx link");
        return;
    }
    if (facebookIntLinkRegex.test(l)) {
        console.debug("result: FB internal link");
        return;
    }
    // facebook redirector -> is an external link
    if (!facebookExtLinkRegex.test(l)) {
        console.debug("non-fb external link");
        l = removeFbclid(l);
        console.debug("Navigating to Link: " + l);
        Qt.openUrlExternally(Qt.resolvedUrl(l));
        return;
    }
    console.debug("External Link: " + l);
    l = removeFbclid(l);
    l = findHyperlink(l);
    console.debug("Mangled Link: " + l);
    Qt.openUrlExternally(Qt.resolvedUrl(l));
}

// ----------------------------------------------------------------

function removeFbclid(u) {
    return u.replace(facebookFbclidRegex, "$1$2");
}

// ----------------------------------------------------------------

// Detect external links
function findHyperlink(link) {
    return facebookExtLinkRegex.exec(decodeURIComponent(link));
}

// ----------------------------------------------------------------

// vim: ft=javascript expandtab ts=4 sw=4 st=4
