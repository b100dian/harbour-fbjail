/* would be nice if we could just use pure JSON in an import, haven't figured out how though.
 * so this is just a JS array.
 * On the plus side, we can use comments here.
 */

var data = [
//    {"name": "Home",           "link": "/home.php",          "iconname": "icon-m-home"},
    {"name": "Notifications",  "link": "/notifications/",    "iconname": "icon-m-notifications"},
    {"name": "Messages",       "link": "/messages/t/",       "iconname": "icon-m-region"},
    {"name": "Groups",         "link": "/groups/",           "iconname": "icon-m-users"},
    {"name": "Friends",        "link": "/friends/",          "iconname": "icon-m-contact"},
//    {"name": "Profile",        "link": "/profile/",          "iconname": "icon-m-media-artists"},
    {"name": "Settings",       "link": "/settings/",         "iconname": "icon-m-setting"},
    {"name": "Log out",        "link": "https://m.facebook.com/login/save-password-interstitial/?ref_component=mbasic_footer", "iconname": "icon-m-close" }
];
