import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Silica.Background 1.0

DockedPanel {
    id: panel
    dock: Dock.Top
    //open: true
    //modal: true
    //clip: true
    animationDuration: 0
    height: col.height
    width: Theme.buttonWidthMedium
    background: ThemeBackground { backgroundMaterial: "glass" ; color: Theme.highlightDimmerColor; opacity: Theme.opacityOverlay}

    Column {
        id: col
        width: parent.width
        anchors.left: parent.left
        spacing: Theme.paddingSmall
        bottomPadding: Theme.paddingSmall
        BackgroundItem {
            Row {
                property bool active: { var re = /^[htps]{3,5}\:\/{2}mbasic\./; return re.test(webview.url.toString()); }
                Icon {
                    source: "image://theme/icon-m-website?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
                    highlighted: parent.active
                    anchors.verticalCenter: parent.verticalCenter
                }
                Label { text: qsTr("Basic", "page render mode"); anchors.verticalCenter: parent.verticalCenter; color: parent.active ? Theme.highlightColor : Theme.primaryColor}
            }
            onClicked: { page.state = page.stateNames.basic; panel.hide()}
        }
        BackgroundItem {
            Row {
                property bool active: { var re = /^[htps]{3,5}\:\/{2}m\./; return re.test(webview.url.toString()); }
                Icon {
                    source: "image://theme/icon-m-device?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
                    highlighted: parent.active
                    anchors.verticalCenter: parent.verticalCenter
                }
                Label { text: qsTr("Mobile", "page render mode"); anchors.verticalCenter: parent.verticalCenter; color: parent.active ? Theme.highlightColor : Theme.primaryColor}
            }
            onClicked: { page.state = page.stateNames.mobile; panel.hide()}
        }
        BackgroundItem {
            Row {
                Icon {
                    source: "image://theme/icon-m-computer?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
                    highlighted: webview.desktopMode
                }
                Label { text: qsTr("Desktop", "page render mode"); anchors.verticalCenter: parent.verticalCenter; color: webview.desktopMode ? Theme.highlightColor : Theme.primaryColor}
            }
            onClicked: { page.state = page.stateNames.desktop; panel.hide()}
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
