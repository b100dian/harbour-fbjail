import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0

SilicaItem {
    id: header
    property string title: webview.title ? webview.title : qsTr("Loading...")
    property string description: page.isPortrait ? webview.url : ""
    property string leftIcon: "image://theme/icon-m-website"
    anchors {
        top: parent.top
        horizontalCenter: parent.horizontalCenter
    }
    width: parent.width

    property int headerHeight: (app.hideui && page.isLandscape ) ? 0 : Theme.iconSizeLarge
    height: headerHeight
    visible: height >= headerHeight
    Behavior on height { NumberAnimation{} }

    IconButton {
        id: nicon
        icon.source: header.leftIcon
        anchors.verticalCenter: textcol.verticalCenter
        anchors.left: parent.left
        //Label { anchors.fill: parent; anchors.centerIn: parent; }
        onClicked: barMenu.open = !barMenu.open
    }
    Column {
        id: textcol
        width: parent.width - (nicon.width + picon.width)
        anchors {
            left: nicon.right
            right: picon.left
            verticalCenter: parent.verticalCenter
        }
        Label {
            text: header.title
            width: parent.width
            horizontalAlignment: Text.AlignLeft
            elide: Text.ElideRight
            color: Theme.secondaryHighlightColor
        }
        Label {
            visible: text.length > 0 // so the "Loading..." default above is centered horizontally
            text: header.description
            width: parent.width
            font.pixelSize: Theme.fontSizeSmall
            horizontalAlignment: Text.AlignLeft
            elide: Text.ElideRight
            color: Theme.secondaryColor
        }
    }
    IconButton {
        id: picon
        icon.source: rightPage.open ? "image://theme/icon-m-events" : "image://theme/icon-m-events?" + Theme.primaryColor
        anchors.verticalCenter: textcol.verticalCenter
        anchors.right: parent.right
        onClicked: rightPage.open = !rightPage.open
    }
    BackgroundItem {
        anchors.fill: textcol
        Notification { id: notification; summary: qsTr("URL copied to Clipboard!") }
        //ShareAction  { id: share; title:  "Share URL" ; mimeType: "text/x-url"; resources: [ web.url ]}
        //WebShareAction  { id: share; }
        //onPressAndHold: share.shareLink(web.url, web.title)
        onPressAndHold: {
            Clipboard.text = webview.url;
            notification.publish();
        }
        onClicked: {
            var dialog = pageStack.push(Qt.resolvedUrl("UrlEditDialog.qml"), { title: qsTr("Edit Url") , what: webview.url });
            dialog.accepted.connect(function(){console.debug("Got from dialog:" + dialog.what); webview.load(dialog.what)})
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
