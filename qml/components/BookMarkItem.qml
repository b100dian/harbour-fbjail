import QtQuick 2.6
import Sailfish.Silica 1.0

ListItem {
    id: bmi
    anchors.horizontalCenter: parent.horizontalCenter
    //contentHeight: Theme.itemSizeMedium //list items with two lines of text
    contentHeight: ( Theme.fontSizeSmall + Theme.fontSizeMedium + Theme.paddingSmall * 4) + Theme.paddingSmall * 2 // two lines plus our own padding...
    ListView.onRemove: animateRemoval(bmi)
    Label {
        id: namelabel
        y: Theme.paddingSmall
        width: parent.width
        text: name
        color: Theme.secondaryHighlightColor
        elide: Text.ElideRight
        wrapMode: Text.NoWrap
    }
    Label {
        anchors.top: namelabel.bottom
        width: parent.width
        text: link.replace(/^[htps]{3,5}\:\/\/(.*)/, "$1")
        color: Theme.secondaryColor
        font.pixelSize: Theme.fontSizeSmall
        elide: Text.ElideRight
        wrapMode: Text.NoWrap
    }
    Separator {
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        //horizontalAlignment: Text.AlignHCenter
        color: Theme.secondaryColor
        visible: !bmi.menuOpen
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
