import QtQuick 2.6
import Sailfish.Silica 1.0

SilicaItem {
    id: footer
    width: parent.width
    //height: Theme.itemSizeSmall

    property int footerHeight: (app.hideui && page.isLandscape ) ? 0 : Theme.itemSizeSmall
    height: footerHeight
    visible: height >= footerHeight
    Behavior on height { NumberAnimation{} }


    anchors {
        bottom: parent.bottom
        horizontalCenter: parent.horizontalCenter
    }
    IconButton {
        anchors.left: parent.left
        icon.source: "image://theme/icon-m-back?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
        onClicked: webview.goBack()
        opacity: webview.canGoBack ? 1 : Theme.opacityFaint
    }
    Row {
        spacing: Theme.paddingMedium
        anchors.centerIn: parent
        IconButton {
            icon.source: "image://theme/icon-m-home?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
            onClicked: webview.load("https://facebook.com/home.php?sk=" + (app.chronoSort? "h_chr" : ""))
        }
        //IconButton {
        //    icon.source: "image://theme/icon-m-website?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
        //    onClicked: page.state = "basic"
        //    highlighted: { var re = /^[htps]{3,5}\:\/{2}mbasic\./; return re.test(webview.url.toString()); }
        //}
        //IconButton {
        //    icon.source: "image://theme/icon-m-device?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
        //    onClicked: page.state = "mobile"
        //    highlighted: { var re = /^[htps]{3,5}\:\/{2}m\./; return re.test(webview.url.toString()); }
        //}
        //IconButton {
        //    icon.source: "image://theme/icon-m-computer?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
        //    onClicked: page.state = "full"
        //    highlighted: webview.desktopMode
        //}
        //IconButton {
        //    icon.source: "image://theme/icon-m-setting?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
        //    onClicked: webview.load("https://facebook.com/profile.php")
        //}
    }
    IconButton {
        anchors.right: fwdbutton.left
        icon.source: webview.loading ?
            "image://theme/icon-m-dismiss?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
            : "image://theme/icon-m-refresh?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
        onClicked: webview.loading ? webview.stop() : webview.reload();
    }
    IconButton {
        id: fwdbutton
        anchors.right: parent.right
        icon.source: "image://theme/icon-m-forward?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
        onClicked: webview.goForward();
        opacity: webview.canGoForward ? 1 : Theme.opacityFaint
    }
    Rectangle {
        anchors.verticalCenter: parent.top
        anchors.left: parent.left
        //color: webview.loading ? webview.backgroundColor : "transparent"
        color: webview.loading ? Theme.highlightDimmerColor : "transparent"
        visible: webview.loading && (webview.loadProgress > 5)
        height: Theme.paddingSmall
        width: parent.width * webview.loadProgress / 100
        z: -1
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
