import QtQuick 2.6
import Sailfish.Silica 1.0
import "../config/fortunes.js" as Fortune

Label {
    visible: (config.nag != false) // dconf setting
    Component.onCompleted: text = Fortune.get()
    horizontalAlignment: Text.AlignHCenter
    color: Theme.secondaryHighlightColor
    font.pixelSize: Theme.fontSizeTiny
    wrapMode: Text.WordWrap
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
