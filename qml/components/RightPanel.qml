import QtQuick 2.6
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0
import Sailfish.Silica.Background 1.0
import Nemo.Notifications 1.0
import "../javascript/database.js" as DB
import "../config/shortcuts.js" as Shortcuts
import "../components"

DockedPanel {
    id: rightpanel
    dock: Dock.Right
    modal: true
    clip: true
    animationDuration : 250
    contentHeight: col.height

    height: parent.height
    width: parent.width -  Theme.horizontalPageMargin * 2

    // workaround for too much transparency: add a Background with a Material
    //background: Background { material: Materials.monochrome; roundedCorners: Corners.All }
    background: ThemeBackground { backgroundMaterial: "glass" ; color: Theme.highlightDimmerColor; radius: Theme.paddingSmall; roundedCorners: Corners.All }

    property string inTitle: ""
    property string inLink: ""

    property ListModel bookmarkModel: ListModel {
        //ListElement{elemid: 0; name: "Trainingsgruppe";   link: "https://www.facebook.com/messages/t/361860070618137/";          iconname: ""}
        function populate() {
            var bmData = DB.listBookmarks();
            for(var i = 0; i < bmData.length; i++) { bookmarkModel.append(bmData[i]); }
        }
    }
    property ListModel navModel: ListModel {
        function populate() {
            //var shortcuts = Shortcuts.data;
            for(var i = 0; i < Shortcuts.data.length; i++) { navModel.append(Shortcuts.data[i]); }
        }
    }
    Component.onCompleted: {
        navModel.populate();
        bookmarkModel.populate();
    }

    Column {
        id: col

        width: parent.width
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.centerIn: parent
        bottomPadding: Theme.paddingLarge

        spacing: Theme.paddingMedium
        SectionHeader { text: qsTr("Shortcuts") }
        Flow {
            width: parent.width
            leftPadding: Theme.horizontalPageMargin
            rightPadding: Theme.horizontalPageMargin
            spacing: Theme.paddingSmall
            anchors.horizontalCenter: parent.horizontalCenter
            //anchors.margins: Theme.paddingMedium
            Repeater {
                model: navModel
                delegate: LinkItem {
                    label: name
                    icon.source: "image://theme/" + (iconname ? iconname : "icon-m-website")
                    button.onClicked: { page.replaceUri(link); rightpanel.hide()}
                }
            }
        }
        SectionHeader { text: qsTr("Bookmarks") }
        SilicaListView {
            id: bmview
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - Theme.horizontalPageMargin * 2
            height: Math.min(Theme.itemSizeMedium * 5, Theme.itemSizeMedium * model.count)
            clip: true

            ScrollDecorator {flickable: bmview }

            model: bookmarkModel
            delegate: BookMarkItem {
                width: ListView.view.width
                function remove() {
                    remorseDelete(function() { hidden=true; DB.deleteBookmark(link) })
                }
                function updatePreference(what) {
                    DB.updatePreference(what, link)
                }
                menu: Component {ContextMenu {
                    MenuLabel { text: link }
                    MenuItem { text: qsTr("Load mobile") ;  onClicked: { updatePreference("m", link); page.state = page.stateNames.mobile;  webview.url = link }}
                    MenuItem { text: qsTr("Load desktop") ; onClicked: { updatePreference("d", link); page.state = page.stateNames.desktop; webview.url = link }}
                    MenuItem { text: qsTr("Remove"); onClicked: remove() }
                }}
                onClicked: webview.url = link
            }
        }
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: Theme.paddingMedium
            text: qsTr("Add")
            Notification { id: notification; summary: qsTr("Adding bookmark failed (duplicate link?)!") }
            onClicked: {
                var r = DB.saveBookmark({"name": inTitle, "link": inLink, "icon": "", "prefer": "b"});
                console.debug("save returned:" , r);
                if (r) {
                    // saveBookmark returns false on exception, so only append if we actually saved something:
                    bookmarkModel.append( {"name": inTitle, "link": inLink, "iconname": ""} );
                } else {
                     notification.publish();
                }
                bmview.scrollToBottom();
            }
        }
        FortuneItem {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
