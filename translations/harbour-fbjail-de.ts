<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="18"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="28"/>
        <source>What&apos;s %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <source>%1 is an unofficial Facebook client for Sailfish OS.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="42"/>
        <source>It is in fact a simple combination of Sailfish OS features WebView and SailJail so your FB browsing is separated from your other browsing activities.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="50"/>
        <source>Hopefully this will improve your privacy a little bit - although of course Facebook/Meta are world-class in spying so additional measures are required for this to actually be effective.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="53"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="60"/>
        <source>%1 is free software released under the MIT License.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="61"/>
        <source>WebView and SailJail are developed by Jolla and the Sailfish OS community.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="62"/>
        <source>WebView includes software developed by Mozilla and is licensed under the Mozilla Public License 2.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="63"/>
        <source>SailJail is licensed under the 3-Clause BSD license and is based on Firejail developed by the NetBlue project licensed under the GPL 2.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="66"/>
        <source>Source Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="74"/>
        <source>available on &lt;a href=&apos;https://gitlab.com/nephros/harbour-fbjail&apos;&gt;GitLab&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="77"/>
        <source>Developers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BarMenu</name>
    <message>
        <location filename="../qml/components/BarMenu.qml" line="30"/>
        <source>Basic</source>
        <comment>page render mode</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/BarMenu.qml" line="42"/>
        <source>Mobile</source>
        <comment>page render mode</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/BarMenu.qml" line="52"/>
        <source>Desktop</source>
        <comment>page render mode</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="30"/>
        <source>Basic</source>
        <comment>page display mode</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="39"/>
        <source>Mobile</source>
        <comment>page display mode</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="48"/>
        <source>Desktop</source>
        <comment>page display mode</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="115"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="116"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RightPanel</name>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="55"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="72"/>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="93"/>
        <source>Load mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="94"/>
        <source>Load desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="95"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="103"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RightPanel.qml" line="104"/>
        <source>Adding bookmark failed (duplicate link?)!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="10"/>
        <source>Settings</source>
        <comment>page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="15"/>
        <source>Hide UI in landscape mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="16"/>
        <source>If enabled, the top and bottom bars are hidden in landscape mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="22"/>
        <source>Remember last page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="23"/>
        <source>If enabled, the last visited page will be loaded after application (re)start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="29"/>
        <source>Show healthy reminders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="30"/>
        <source>If enabled, some parts of the UI will show helpful reminders about Facebook usage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="35"/>
        <source>Page Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="40"/>
        <source>Zoom factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="53"/>
        <source>Restart the App to apply zoom factor changes.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TopBar</name>
    <message>
        <location filename="../qml/components/TopBar.qml" line="7"/>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/TopBar.qml" line="63"/>
        <source>URL copied to Clipboard!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/TopBar.qml" line="72"/>
        <source>Edit Url</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UrlEditDialog</name>
    <message>
        <location filename="../qml/components/UrlEditDialog.qml" line="33"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
