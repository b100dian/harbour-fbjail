#!/bin/sh

#./update_translations.sh qm || exit 1
printf 'specifying...\n'
specify -Nns rpm/*yaml >/dev/null || exit 1
#if [[ ! x"$1" = x"release" ]] ; then
#  gtag=$(git rev-parse --short HEAD).$(git branch --show-current | sed 's/-/_/')
#  sed -i -e "s/\(Release: \).*$/\1 0\+$gtag/" rpm/*.spec
#fi
printf 'linting...\n'
for f in $(find qml -name "*.qml" -o -name "*.js" ); do
 /usr/lib/qt5/bin/qmllint "$f" || exit 1
done
printf 'building...\n'
rpmbuild -bb --build-in-place rpm/*.spec > build.log 2>&1
printf "exit: $?\n"
awk '/^Wrote/ {print $NF}' build.log
#if [[ ! x"$1" = x"release" ]] ; then
#  git restore rpm/*spec > /dev/null
  printf 'installing...\n'
  awk '/^Wrote/ {print $NF}' build.log | xargs xdg-open
#else
#  git tag -f release-build
#fi
